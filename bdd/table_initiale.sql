drop table if exists work.coriandre_init;
create table work.coriandre_init (
	id serial,
	id_coriandre text, -- mode de construction à definir
	categorie_coriandre text, -- nomenclature Coriandre
	-- données sur la source brute et sur l'objet
	source text,
	id_source text,
	libelle text,
	-- données de géolocalisation de la source brute
	x_brut decimal, -- 4326
	y_brut decimal,  -- 4326
	idpar_brut text[],
	adresse_brut text,
	siret_brut text,
	-- données de geolocalisation consolidées autant que possible 
	x_fiab decimal,  -- 4326
	y_fiab decimal,  -- 4326
	geomloc_fiab geometry(point), -- issu des 2 infos précédentes
	idpar_fiab text[],
	adresse_fiab text,
	geompar_fiab geometry(multipolygon), -- issu de idpar_fiab
	niveau_fiab text,
	-- données de rapprochement à BDNB / FF / BDTOPO
	batiment_groupe_id text,
	version_bdnb text,
	idtup text,
	version_ff text,
	bdtopo_bat_cleaps text,
	version_bdtopo text,
	-- champs metier
	cat_bati_bdtopo text,
	cat_proprio_type text	
	-- reste a definir par la suite
);
comment on column work.coriandre_init.id is 'Identifiant entier';
comment on column work.coriandre_init.id_coriandre is 'Identifiant Coriandre';
comment on column work.coriandre_init.categorie_coriandre  is 'Categorie de la nomenclature Coriandre';
comment on column work.coriandre_init.source is 'Source de données mobilisée';
comment on column work.coriandre_init.id_source is 'Identifiant de l''entité dans la source brute';
comment on column work.coriandre_init.libelle is 'Libelle de l''entité dans la source brute';
comment on column work.coriandre_init.x_brut is 'X de l''entité brute si existant (EPSG 4326)';
comment on column work.coriandre_init.y_brut is 'Y de l''entité brute si existant (EPSG 4326)';
comment on column work.coriandre_init.idpar_brut is 'Liste des identifiants de parcelle bruts si existants';
comment on column work.coriandre_init.adresse_brut  is 'Adresse brute si existante';
comment on column work.coriandre_init.siret_brut  is 'SIRET brut si existant';
comment on column work.coriandre_init.x_fiab is 'X calculé (EPSG 4326)';
comment on column work.coriandre_init.y_fiab  is 'Y calculé (EPSG 4326)';
comment on column work.coriandre_init.geomloc_fiab  is 'Point calculé (EPSG 4326)';
comment on column work.coriandre_init.idpar_fiab is 'Liste des identifiants de parcelle calculés';
comment on column work.coriandre_init.adresse_fiab  is 'Adresse fiabilisée (BAN)';
comment on column work.coriandre_init.geompar_fiab  is 'Géometrie des parcelles';
comment on column work.coriandre_init.niveau_fiab is 'Estimation de la fiabilité de la geolocalisation (echelle 0 à 5)';
comment on column work.coriandre_init.batiment_groupe_id  is 'Id Batiment groupe';
comment on column work.coriandre_init.version_bdnb  is 'Version BDNB utilisée';
comment on column work.coriandre_init.idtup  is 'Idtup';
comment on column work.coriandre_init.version_ff  is 'Millesime FF utilisé';
comment on column work.coriandre_init.bdtopo_bat_cleaps  is 'Id Bdtopo';
comment on column work.coriandre_init.version_bdtopo  is 'Version BDTOPO utilisée';
comment on column work.coriandre_init.cat_bati_bdtopo  is 'Caractérisation du batiment selon la bdtopo';
comment on column work.coriandre_init.cat_proprio_type  is 'Categorie du propriétaire de la TUP selon FF';