-- Table: etablissement_publique
CREATE TABLE etablissement_publique (etablissement_id character varying, nom_etablissement character varying, siren character varying, statut_public boolean, categorie_2 character varying, categorie_3 character varying, code_departement_insee character varying, fiabilite character varying, surface character varying, date_ouverture character varying, PRIMARY KEY (etablissement_id));
CREATE UNIQUE INDEX etablissement_publique_pk_id ON etablissement_publique USING btree (etablissement_id);
CREATE INDEX etablissement_publique_etablissement_id_idx ON etablissement_publique USING btree (etablissement_id);
CREATE INDEX etablissement_publique_code_departement_insee_idx ON etablissement_publique USING btree (code_departement_insee);

-- Table: rel_batiment_groupe_etablissement_publique
CREATE TABLE rel_batiment_groupe_etablissement_publique (batiment_groupe_id character varying, etablissement_id character varying, fiabilite_appariement text, code_departement_insee character varying, PRIMARY KEY (batiment_groupe_id, etablissement_id));
CREATE UNIQUE INDEX rel_batiment_groupe_etablissement_publique_pk_id ON rel_batiment_groupe_etablissement_publique USING btree (batiment_groupe_id, etablissement_id);
CREATE INDEX rel_batiment_groupe_etablissement_publique_batiment_groupe_id_i ON rel_batiment_groupe_etablissement_publique USING btree (batiment_groupe_id);
CREATE INDEX rel_batiment_groupe_etablissement_publique_etablissement_id_idx ON rel_batiment_groupe_etablissement_publique USING btree (etablissement_id);
CREATE INDEX rel_batiment_groupe_etablissement_publique_code_departement_ins ON rel_batiment_groupe_etablissement_publique USING btree (code_departement_insee);

-- Table: rel_batiment_groupe_source_etablissement_publique
CREATE TABLE rel_batiment_groupe_source_etablissement_publique (batiment_groupe_id character varying, id_source character varying, source character varying, code_departement_insee character varying, statut_public boolean, categorie_3 text, fiabilite_appariement text, rang_batiment_group_category integer, rang_id_source integer, PRIMARY KEY (batiment_groupe_id, id_source));
CREATE UNIQUE INDEX rel_batiment_groupe_source_etablissement_publique_pk_id ON rel_batiment_groupe_source_etablissement_publique USING btree (batiment_groupe_id, id_source);
CREATE INDEX rel_batiment_groupe_source_etablissement_publique_batiment_grou ON rel_batiment_groupe_source_etablissement_publique USING btree (batiment_groupe_id);
CREATE INDEX rel_batiment_groupe_source_etablissement_publique_id_source_idx ON rel_batiment_groupe_source_etablissement_publique USING btree (id_source);
CREATE INDEX rel_batiment_groupe_source_etablissement_publique_code_departem ON rel_batiment_groupe_source_etablissement_publique USING btree (code_departement_insee);

-- Table: rel_source_etablissement_publique
CREATE TABLE rel_source_etablissement_publique (id_source character varying, etablissement_id character varying, source character varying, code_departement_insee character varying, rang_id_source integer, PRIMARY KEY (id_source));
CREATE UNIQUE INDEX rel_source_etablissement_publique_pk_id ON rel_source_etablissement_publique USING btree (id_source);
CREATE INDEX rel_source_etablissement_publique_id_source_idx ON rel_source_etablissement_publique USING btree (id_source);
CREATE INDEX rel_source_etablissement_publique_code_departement_insee_idx ON rel_source_etablissement_publique USING btree (code_departement_insee);

-- Table: rel_batiment_groupe_pies
CREATE TABLE rel_batiment_groupe_pies (batiment_groupe_id character varying, id_pies character varying, cle_interop_adr text, batiment_construction_id text, parcelle_unifiee_id text, type_appariement text, code_departement_insee character varying, fiabilite_geocodage text, fiabilite_appariement text, distance_adr integer, distance_geom_pies integer, PRIMARY KEY (batiment_groupe_id, id_pies));
CREATE UNIQUE INDEX rel_batiment_groupe_pies_pk_id ON rel_batiment_groupe_pies USING btree (batiment_groupe_id, id_pies);
CREATE INDEX rel_batiment_groupe_pies_code_departement_insee_idx ON rel_batiment_groupe_pies USING btree (code_departement_insee);
CREATE INDEX rel_batiment_groupe_pies_id_pies_idx ON rel_batiment_groupe_pies USING btree (id_pies);

-- Table: rel_batiment_groupe_pees
CREATE TABLE rel_batiment_groupe_pees (batiment_groupe_id character varying, id_pees character varying, cle_interop_adr text, batiment_construction_id text, parcelle_unifiee_id text, type_appariement text, code_departement_insee character varying, fiabilite_geocodage text, fiabilite_appariement text, distance_adr integer, distance_geom_pees integer, PRIMARY KEY (batiment_groupe_id, id_pees));
CREATE UNIQUE INDEX rel_batiment_groupe_pees_pk_id ON rel_batiment_groupe_pees USING btree (batiment_groupe_id, id_pees);
CREATE INDEX rel_batiment_groupe_pees_code_departement_insee_idx ON rel_batiment_groupe_pees USING btree (code_departement_insee);
CREATE INDEX rel_batiment_groupe_pees_id_pees_idx ON rel_batiment_groupe_pees USING btree (id_pees);

-- Table: rel_batiment_groupe_finess
CREATE TABLE rel_batiment_groupe_finess (batiment_groupe_id character varying, id_finess character varying, cle_interop_adr text, batiment_construction_id text, parcelle_unifiee_id text, type_appariement text, code_departement_insee character varying, fiabilite_geocodage text, fiabilite_appariement text, distance_adr integer, distance_geom_finess integer, PRIMARY KEY (batiment_groupe_id, id_finess));
CREATE UNIQUE INDEX rel_batiment_groupe_finess_pk_id ON rel_batiment_groupe_finess USING btree (batiment_groupe_id, id_finess);
CREATE INDEX rel_batiment_groupe_finess_code_departement_insee_idx ON rel_batiment_groupe_finess USING btree (code_departement_insee);
CREATE INDEX rel_batiment_groupe_finess_id_finess_idx ON rel_batiment_groupe_finess USING btree (id_finess);

-- Table: rel_batiment_groupe_aen
CREATE TABLE rel_batiment_groupe_aen (batiment_groupe_id character varying, id_interne_aen character varying, cle_interop_adr text, batiment_construction_id text, parcelle_unifiee_id text, type_appariement text, code_departement_insee character varying, fiabilite_geocodage text, fiabilite_appariement text, distance_adr integer, distance_geom_aen integer, PRIMARY KEY (batiment_groupe_id, id_interne_aen));
CREATE UNIQUE INDEX rel_batiment_groupe_aen_pk_id ON rel_batiment_groupe_aen USING btree (batiment_groupe_id, id_interne_aen);
CREATE INDEX rel_batiment_groupe_aen_code_departement_insee_idx ON rel_batiment_groupe_aen USING btree (code_departement_insee);
CREATE INDEX rel_batiment_groupe_aen_id_interne_aen_idx ON rel_batiment_groupe_aen USING btree (id_interne_aen);

-- Table: rel_batiment_groupe_adm
CREATE TABLE rel_batiment_groupe_adm (batiment_groupe_id character varying, id_adm character varying, cle_interop_adr text, batiment_construction_id text, parcelle_unifiee_id text, type_appariement text, code_departement_insee character varying, fiabilite_geocodage text, fiabilite_appariement text, distance_adr integer, distance_geom_adm integer, PRIMARY KEY (batiment_groupe_id, id_adm));
CREATE UNIQUE INDEX rel_batiment_groupe_adm_pk_id ON rel_batiment_groupe_adm USING btree (batiment_groupe_id, id_adm);
CREATE INDEX rel_batiment_groupe_adm_code_departement_insee_idx ON rel_batiment_groupe_adm USING btree (code_departement_insee);
CREATE INDEX rel_batiment_groupe_adm_id_adm_idx ON rel_batiment_groupe_adm USING btree (id_adm);

-- Table: rel_batiment_groupe_bpe21
CREATE TABLE rel_batiment_groupe_bpe21 (batiment_groupe_id character varying, bpe_interne_id character varying, batiment_construction_id text, parcelle_unifiee_id text, type_appariement text, code_departement_insee character varying, fiabilite_appariement text, distance_geom_bpe integer, PRIMARY KEY (batiment_groupe_id, bpe_interne_id));
CREATE UNIQUE INDEX rel_batiment_groupe_bpe21_pk_id ON rel_batiment_groupe_bpe21 USING btree (batiment_groupe_id, bpe_interne_id);
CREATE INDEX rel_batiment_groupe_bpe21_code_departement_insee_idx ON rel_batiment_groupe_bpe21 USING btree (code_departement_insee);
CREATE INDEX rel_batiment_groupe_bpe21_bpe_interne_id_idx ON rel_batiment_groupe_bpe21 USING btree (bpe_interne_id);
